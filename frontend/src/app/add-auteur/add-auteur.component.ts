import { Component, OnInit } from '@angular/core';
import {AuteurService} from '../services/auteur.service';
import {Author} from '../model/author';
import {Router} from '@angular/router';
import {NotifierModule, NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-add-auteur',
  templateUrl: './add-auteur.component.html',
  styleUrls: ['./add-auteur.component.scss']
})
export class AddAuteurComponent implements OnInit {
  listAuthor!: Author[];
  author = new Author();
  private readonly notifier: NotifierService;

  constructor(private authorService: AuteurService, private router: Router, notifierService: NotifierService) {
    this.notifier = notifierService;
  }
  ngOnInit(): void {
  }
  save(): void {
    this.authorService.postAuthor(this.author).subscribe(data => {
      console.log('data', data);
      this.listAuthor.push(data);
      this.router.navigateByUrl('/auteur');
    });
    this.notifier.notify('success', 'Author added');
  }


}
