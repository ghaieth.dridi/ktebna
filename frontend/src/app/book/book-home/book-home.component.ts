import { Component, OnInit } from '@angular/core';
import {Book} from '../book';
import {BookService} from '../book.service';
import {AuteurService} from '../../services/auteur.service';
import {Author} from "../../model/author";


@Component({
  selector: 'app-book-home',
  templateUrl: './book-home.component.html',
  styleUrls: ['./book-home.component.scss']
})
export class BookHomeComponent implements OnInit {

  listBook!: Book[];
  book!: Book;
  book1!: Book;
  currentAuthor: Author = new Author();
  term = '';
  constructor(public bookService: BookService, public authorService: AuteurService) {
  }

  getAll(): void {
    this.bookService.getAll().subscribe(data => {
    this.listBook = data;
    console.log(this.listBook);
    }, error => {
      console.log(error);
    });

  }
  delete(id: any): void {
    this.bookService.delete(id).subscribe(data => {
      this.getAll();
    });
  }
  ngOnInit(): void {
    this.book = new class implements Book {
      adders: any;
      categories: any;
      cover: any;
      description: any;
      id: any;
      idAuthor: any;
      language: any;
      orders: any;
      owners: any;
      photo: any;
      price: any;
      title: any;
      value: any;
      year: any;
    }();
    this.getAll();
    this.book1 = new Book();
  }
  update(book: any): void {
    this.bookService.update(this.book).subscribe(data => {
    });
  }

  getOneAuther(id: any): void{
  this.bookService.getByIdAuth(id).subscribe(data => {
    this.currentAuthor = data;
    console.log(data);
  });
  console.log(this.currentAuthor);
}
  getByid(book: any): void {
    this.book1 = Object.create(book);
    this.getOneAuther(this.book1.idAuthor);
  }



}

